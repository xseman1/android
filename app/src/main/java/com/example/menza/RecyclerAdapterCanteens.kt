package com.example.menza

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapterCanteens(private val context: Context, private val newSelection: Boolean) : RecyclerView.Adapter<RecyclerAdapterCanteens.ViewHolder>() {
    private var listOfCanteens = mutableListOf<Canteen>()

    fun setListData(data: MutableList<Canteen>) {
        listOfCanteens = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.canteen_row_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val menza = listOfCanteens[position]
        holder.bindView(menza)
    }

    override fun getItemCount(): Int {
        if (listOfCanteens.size > 0)
            return listOfCanteens.size
        else
            return 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var canteenName = itemView.findViewById<TextView>(R.id.name_of_canteen)

        init {
            itemView.setOnClickListener{
                val sharedPreferences = (context as Activity).getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.apply {
                    putString("ID_KEY", canteenName.hint.toString())
                    commit()
                }

                if (!newSelection){
                    var intent = Intent(itemView.context, CanteenActivity::class.java)
                    ContextCompat.startActivity(itemView.context, intent, null)
                }
                else {

                }

                context.finish()
            }
        }

        fun bindView(canteen: Canteen) {
            canteenName.text = canteen.name
            canteenName.hint = canteen.key
        }
    }
}