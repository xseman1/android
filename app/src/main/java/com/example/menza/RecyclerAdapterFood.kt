package com.example.menza

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class RecyclerAdapterFood(private val context: Context) : RecyclerView.Adapter<RecyclerAdapterFood.ViewHolder>() {
    private var menu = mutableListOf<Food>()

    fun setListData(data: MutableList<Food>) {
        menu = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.menu_row_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val jedla = menu[position]
        holder.bindView(jedla)
    }

    override fun getItemCount(): Int {
        if (menu.size > 0)
            return menu.size
        else
            return 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var foodName: TextView = itemView.findViewById(R.id.food_name)
        var statusSwitch: Switch = itemView.findViewById(R.id.status)
        var card: CardView = itemView.findViewById(R.id.card)
        lateinit var canteen : String
        lateinit var key : String
        lateinit var day : String

        init {
            statusSwitch.setOnClickListener{
                showConfirmationAlert(true)
            }
            card.setOnClickListener{
                showConfirmationAlert(false)
            }
        }

        fun bindView(food: Food) {
            foodName.text = food.name
            canteen = food.canteen
            key = food.key
            day = food.day

            statusSwitch.isChecked = food.status
        }

        private fun changeAvailability() {
            var map = mutableMapOf<String, Any>()

            var db = FirebaseDatabase.getInstance().getReference("canteens").child(canteen).child("menu").child(day).child("menuItem")
            db.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (data in snapshot.children) {
                        if (data.key.equals(key)) {
                            map["available"] = !(data.child("available").value as Boolean)
                            db.child(key).updateChildren(map)
                        }
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
            })
        }
        private fun reverseClick(clickedSwitch: Boolean) {
            if (clickedSwitch)
                statusSwitch.isChecked = !statusSwitch.isChecked
        }

        private fun showConfirmationAlert(clickedSwitch: Boolean){
            val builder: AlertDialog.Builder = AlertDialog.Builder(context)
            builder.setTitle("Změna stavu jídla")
            builder.setMessage("Opravdu chcete změnit dostupnost jídla ${foodName.text} ?")

            builder.setPositiveButton("Ano") { dialog, which -> changeAvailability() }
            builder.setNegativeButton("Ne") { dialog, which -> reverseClick(clickedSwitch) }

            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }
}