package com.example.menza

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import java.text.DateFormat
import java.util.*

class CanteenActivity : AppCompatActivity() {

    private lateinit var db : DatabaseReference
    private lateinit var adapter : RecyclerAdapterFood
    private var canteenID: String? = ""
    private val menu = mutableListOf<Food>()
    private lateinit var day : String
    private lateinit var connection: Connection

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_canteen)
        activityLogic()
    }

    override fun onResume() {
        super.onResume()
        activityLogic()
    }

    private fun activityLogic(){
        connection = Connection(applicationContext, this, findViewById(R.id.CanteenCoordinatorLayout))
        canteenID = loadCanteenID()
        db = FirebaseDatabase.getInstance().getReference("canteens").child(canteenID.toString())
        adapter = RecyclerAdapterFood(this)
        findViewById<RecyclerView>(R.id.menu_rw).layoutManager = LinearLayoutManager(this)
        findViewById<RecyclerView>(R.id.menu_rw).adapter = adapter
        getCurrentDayOfTheWeek()
        loadCanteenName()
        loadMenu()
        selectNewCanteen()
    }

    private fun loadCanteenName() {
        db.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (data in snapshot.children) {
                    if (data.key.toString() == "name")
                        findViewById<TextView>(R.id.canteen_name).text = data.value.toString()
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun loadMenu() {
        db = db.child("menu").child(day).child("menuItem")
        db.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                menu.clear()
                for (data in snapshot.children) {
                    menu.add(Food(canteenID.toString(), day, data.key.toString(), data.child("meal").child("name").value.toString(), data.child("available").value as Boolean))
                }
                if (menu.isNotEmpty()) {
                    findViewById<TextView>(R.id.note).text = ""
                    adapter.setListData(menu)
                    adapter.notifyDataSetChanged()
                }
                else
                    findViewById<TextView>(R.id.note).text = "Jídelníček není k dispozici."
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun getCurrentDayOfTheWeek() {
        var date = DateFormat.getDateInstance(DateFormat.FULL).format(Calendar.getInstance().getTime())
        var list = date.split(" ")

        when (list[0]) {
            "pondělí" -> day = "monday"
            "úterý" -> day = "tuesday"
            "středa" -> day = "wednesday"
            "čtvrtek" -> day = "thursday"
            "pátek" -> day = "friday"
            "sobota" -> day = "saturday"
            "neděle" ->  day = "sunday"
        }

        findViewById<TextView>(R.id.date).text = date
    }

    private fun selectNewCanteen() {
        findViewById<Button>(R.id.back).setOnClickListener{
            val sharedPreferences = getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.apply {
                putBoolean("NEW_CANTEEN_SELECTION", true)
                commit()
            }

            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun loadCanteenID(): String {
        val sharedPreferences = getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE)
        return sharedPreferences.getString("ID_KEY", null).toString()
    }
}