package com.example.menza

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import com.google.android.material.snackbar.Snackbar


class Connection(var context: Context, var owner: LifecycleOwner, var view: View) {
    private var connectedWifi: Snackbar = Snackbar.make(view, "Připojeno na internet přes WiFi", Snackbar.LENGTH_SHORT)
    private var connectedMobile: Snackbar = Snackbar.make(view, "Připojeno na internet přes mobilní data", Snackbar.LENGTH_SHORT)
    private var disconnected: Snackbar = Snackbar.make(view, "Bez připojení na internet", Snackbar.LENGTH_INDEFINITE)

    init {
        val snackbarWifiView: View = connectedWifi.view
        (snackbarWifiView.findViewById<View>(R.id.snackbar_text) as TextView).setTextColor(Color.WHITE)
        snackbarWifiView.setBackgroundColor(Color.parseColor("#79BE15"))

        val snackbarMobileView: View = connectedMobile.view
        (snackbarMobileView.findViewById<View>(R.id.snackbar_text) as TextView).setTextColor(Color.WHITE)
        snackbarMobileView.setBackgroundColor(Color.parseColor("#79BE15"))

        val snackbarDisconnectedView: View = disconnected.view
        (snackbarDisconnectedView.findViewById<View>(R.id.snackbar_text) as TextView).setTextColor(Color.WHITE)
        snackbarDisconnectedView.setBackgroundColor(Color.RED)

        checkConnection()
    }

    private fun checkConnection(){
        val connectionLiveData = ConnectionLiveData(context)
        connectionLiveData.observe(owner, { connection ->
            if (connection.getIsConnected()) {
                if (disconnected.isShown) {
                    disconnected.dismiss()

                    if (connection.getType() == 1)
                        connectedWifi.show()
                    else if (connection.getType() == 2)
                        connectedMobile.show()
                }
            } else
                disconnected.show()
        })
    }
}