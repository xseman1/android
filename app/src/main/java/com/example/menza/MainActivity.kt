@file:Suppress("DEPRECATION")

package com.example.menza

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*


class MainActivity : AppCompatActivity() {
    private lateinit var db : DatabaseReference
    private lateinit var adapter : RecyclerAdapterCanteens
    private val listOfCanteens = mutableListOf<Canteen>()
    private lateinit var connection: Connection

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activityLogic()
    }

    private fun activityLogic(){
        connection = Connection(applicationContext, this, findViewById(R.id.MainCoordinatorLayout))
        db = FirebaseDatabase.getInstance().getReference("canteenNames")
        adapter = RecyclerAdapterCanteens(this, newCanteenSelection())
        findViewById<RecyclerView>(R.id.canteens_rw).layoutManager = LinearLayoutManager(this)
        findViewById<RecyclerView>(R.id.canteens_rw).adapter = adapter
        loadCanteens()
        findViewById<RecyclerView>(R.id.canteens_rw).layoutManager = AutoFitGridLayoutManager(this, resources.getDimension(R.dimen.card_width).toInt())
    }

    private fun loadCanteens() {
        db.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                listOfCanteens.clear()
                for (data in snapshot.children) {
                    listOfCanteens.add(Canteen(data.key.toString(), data.value.toString().trim()))
                }
                adapter.setListData(listOfCanteens)
                adapter.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun newCanteenSelection(): Boolean {
        val sharedPreferences = getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE)
        return sharedPreferences.contains("NEW_CANTEEN_SELECTION")
    }
}