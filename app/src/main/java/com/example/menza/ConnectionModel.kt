package com.example.menza

class ConnectionModel(type: Int, isConnected: Boolean) {
    private var type = type
    private var isConnected = isConnected

    fun getType(): Int {
        return type
    }

    fun getIsConnected(): Boolean {
        return isConnected
    }
}