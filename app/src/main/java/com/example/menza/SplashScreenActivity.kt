package com.example.menza

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!canteenSelected())
            startActivity(Intent(this@SplashScreenActivity, MainActivity::class.java))
        else
            startActivity(Intent(this@SplashScreenActivity, CanteenActivity::class.java))

        finish()
    }

    private fun canteenSelected(): Boolean {
        val sharedPreferences = getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE)
        return sharedPreferences.contains("ID_KEY")
    }
}